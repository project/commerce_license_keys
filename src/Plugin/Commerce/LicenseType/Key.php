<?php

namespace Drupal\commerce_license_keys\Plugin\Commerce\LicenseType;

use Drupal\commerce\EntityHelper;
use Drupal\commerce_license\Entity\LicenseInterface;
use Drupal\commerce_license\Plugin\Commerce\LicenseType\LicenseTypeBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides a license type that allocates a key to the new license.
 *
 * @CommerceLicenseType(
 *   id = "key",
 *   label = @Translation("Key"),
 * )
 */
class Key extends LicenseTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(LicenseInterface $license) {
    $args = [
      '@title' => $license->getPurchasedEntity()->label(),
    ];
    return $this->t('@title key', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'available_keys' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function grantLicense(LicenseInterface $license) {
    // @todo Log this, as it's something admins should see?
  }

  /**
   * {@inheritdoc}
   */
  public function revokeLicense(LicenseInterface $license) {
    // @todo Log this, as it's something admins should see?
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['available_keys'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Available keys'),
      '#description' => $this->t('Enter one key per line. Keys will be allocated from the top of the list. If no keys remain, the product variation will not be purchasable.'),
      '#default_value' => isset($this->configuration['available_keys']) ? $this->configuration['available_keys'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);

    if (!empty($values['available_keys'])) {
      $this->configuration['available_keys'] = $values['available_keys'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setConfigurationValuesOnLicense(LicenseInterface $license) {
    $value = 'ERROR';

    if (!empty($this->configuration['available_keys'])) {
      $available_keys = explode("\n", $this->configuration['available_keys']);
      $value = trim(array_shift($available_keys));
    }

    $license->license_key = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['license_key'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Key'))
      ->setDescription(t('The key allocated to this license.'))
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
